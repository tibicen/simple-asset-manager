import os
from platform import platform
import subprocess
from math import pi, cos, sin, radians
import bpy  # type: ignore
from bpy.props import BoolProperty, StringProperty  # type: ignore 
import bmesh  # type: ignore
from mathutils import Euler  # type: ignore

from . import FORMATS, __name__
from . import find_layer, append_element, append_material, append_hdr, append_particles
from . import preview_collections


PREVIEW_RESOLUTION = 500

def purge(data):
    # RENDER PREVIEW SCENE PREPARATION METHODS
    for el in data:
        if el.users == 0:
            data.remove(el)


def prepare_scene():
    pref = bpy.context.preferences.addons[__name__].preferences
    # RENDER PREVIEW SET SCENE
    for ob in bpy.data.objects:
        ob.hide_select = False
        ob.hide_render = False
        ob.hide_viewport = False
        ob.hide_set(False)
        bpy.data.objects.remove(ob)
    for coll in bpy.data.collections:
        coll.hide_select = False
        coll.hide_render = False
        coll.hide_viewport = False
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete(use_global=True)
    purge(bpy.data.collections)
    purge(bpy.data.objects)
    purge(bpy.data.cameras)
    purge(bpy.data.lights)
    purge(bpy.data.meshes)
    purge(bpy.data.particles)
    purge(bpy.data.materials)
    purge(bpy.data.textures)
    purge(bpy.data.images)
    purge(bpy.data.collections)
    # set output
    render = bpy.context.scene.render
    render.film_transparent = True
    render.resolution_x = PREVIEW_RESOLUTION
    render.resolution_y = PREVIEW_RESOLUTION
    render.use_stamp_date = False
    render.use_stamp_render_time = False
    render.use_stamp_camera = False
    render.use_stamp_scene = False
    render.use_stamp_filename = False
    render.use_stamp_frame = False
    render.use_stamp_time = False
    render.use_stamp = True
    render.use_stamp_note = True
    render.stamp_font_size = 50
    render.image_settings.file_format = 'PNG'
    render.image_settings.color_mode = 'RGBA'
    # # add hdr for render
    # b_path = os.path.dirname(bpy.app.binary_path)
    # ver = '{}.{}'.format(*bpy.app.version[:2])
    resource_path = bpy.utils.resource_path('LOCAL')
    hdr = pref.render_env
    hdr_path = os.path.join(resource_path, 'datafiles', 'studiolights', 'world', hdr)
    append_hdr(hdr_path)



def setup_cycles():
    render = bpy.context.scene.render
    cycles = bpy.context.scene.cycles
    render.engine = 'CYCLES'
    render.tile_x = PREVIEW_RESOLUTION
    render.tile_y = PREVIEW_RESOLUTION
    cycles.device = 'CPU'
    cycles.samples = 32
    bpy.context.view_layer.cycles.use_denoising = True


def setup_eevee():
    render = bpy.context.scene.render
    eevee = bpy.context.scene.eevee
    render.engine = 'BLENDER_EEVEE'
    eevee.use_ssr_refraction = True
    eevee.use_ssr = True
    eevee.use_gtao = True
    eevee.gtao_distance = 1
    eevee.taa_render_samples = 16


def add_camera():
    cam = bpy.data.cameras.new('SAM_cam')
    cam_ob = bpy.data.objects.new('SAM_cam_ob', cam)
    bpy.context.collection.objects.link(cam_ob)
    cam_ob.rotation_euler = (pi / 2, 0, -pi / 6)
    cam.shift_y = -.3
    cam.lens = 71
    bpy.data.scenes[0].camera = cam_ob
    bpy.ops.view3d.camera_to_view_selected()
    return cam_ob


def rot_point(point, angle):
    angle = radians(angle)
    x, y = point
    rx = x * cos(angle) - y * sin(angle)
    ry = x * sin(angle) + y * cos(angle)
    return (rx, ry)


def rotate_uv(ob, angle):
    UV = ob.data.uv_layers[0]
    for v in ob.data.loops:
        UV.data[v.index].uv = rot_point(UV.data[v.index].uv, angle)


def set_scene_material(blendFile, cam):
    bpy.ops.mesh.primitive_uv_sphere_add(
        segments=64,
        ring_count=32,
        location=(0, 0, 0),
    )
    ob = bpy.context.active_object
    bpy.ops.object.editmode_toggle()
    bpy.ops.uv.sphere_project(direction='ALIGN_TO_OBJECT')
    mesh = bmesh.from_edit_mesh(ob.data)
    for v in mesh.verts:
        v.select = True if v.co[1] < 0 else False
    bmesh.update_edit_mesh(ob.data)
    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.uv.unwrap(method='CONFORMAL', margin=0)
    # bpy.ops.uv.pack_islands()
    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.uv.unwrap(method='CONFORMAL', margin=0)
    # bpy.ops.uv.pack_islands()
    bpy.ops.object.editmode_toggle()
    rotate_uv(ob, 46.5)
    bpy.ops.object.shade_smooth()
    bpy.ops.object.material_slot_add()
    # append material
    files = append_material(blendFile)
    name = files[0]['name']
    mat = bpy.data.materials[name]
    ob.material_slots[0].material = mat
    # modify camera settings
    cam.rotation_euler = Euler((pi / 2, 0, 0), 'XYZ')
    cam.data.shift_y = 0
    cam.data.lens = 41


def set_scene_hdr(path, cam):
    append_hdr(path)
    # Add spheres and plane
    positions = ((0, 0, 1), (-2.2, 0, 1), (2.2, 0, 1))
    mats = ['Diffuse', 'Mirror', 'Glass']
    for pos in positions:
        bpy.ops.mesh.primitive_uv_sphere_add(
            segments=64,
            ring_count=32,
            location=pos,
        )
        bpy.ops.object.shade_smooth()
        bpy.ops.object.material_slot_add()
        ob = bpy.context.active_object
        mat = bpy.data.materials.new(mats.pop(0))
        mat.use_nodes = True
        ob.material_slots[0].material = mat
    bpy.ops.mesh.primitive_plane_add()
    bpy.context.active_object.scale = (22, 6, 6)
    # Add materials
    diffuse = bpy.data.materials['Diffuse'].node_tree.nodes['Principled BSDF']
    diffuse.inputs['Metallic'].default_value = 0
    diffuse.inputs['Roughness'].default_value = 1
    mirror = bpy.data.materials['Mirror'].node_tree.nodes['Principled BSDF']
    mirror.inputs['Metallic'].default_value = 1
    mirror.inputs['Roughness'].default_value = 0
    glass_ntree = bpy.data.materials['Glass'].node_tree
    glass = glass_ntree.nodes.new('ShaderNodeBsdfGlass')
    output = glass_ntree.nodes['Material Output']
    glass_ntree.links.new(output.inputs[0], glass.outputs[0])
    # Camera and scene
    cam.rotation_euler = Euler((pi / 2, 0, 0), 'XYZ')
    cam.data.shift_y = 0
    cam.data.lens = 41
    cam.location = (0, -9., 3.)
    # Add render settings cycles
    setup_cycles()
    ## Compositor
    bpy.context.scene.use_nodes = True
    tree = bpy.context.scene.node_tree
    # hdr image
    image_node = tree.nodes.new('CompositorNodeImage')
    file = os.path.basename(path)
    im = bpy.data.images[file]
    image_node.image = im
    # alpha node
    alpha = tree.nodes.new('CompositorNodeAlphaOver')
    scale = tree.nodes.new('CompositorNodeScale')
    scale.inputs['X'].default_value = PREVIEW_RESOLUTION
    scale.inputs['Y'].default_value = int(PREVIEW_RESOLUTION * 0.75)
    scale.space = 'ABSOLUTE'
    move = tree.nodes.new('CompositorNodeTranslate')
    move.inputs['Y'].default_value = int(PREVIEW_RESOLUTION * 0.12)
    # links
    tree.links.new(scale.inputs[0], image_node.outputs[0])
    tree.links.new(move.inputs[0], scale.outputs[0])
    tree.links.new(alpha.inputs[2], tree.nodes['Render Layers'].outputs[0])
    tree.links.new(alpha.inputs[1], move.outputs[0])
    tree.links.new(tree.nodes['Composite'].inputs[0], alpha.outputs[0])


def set_scene_particle_settings(blendFile):
    lay_coll = find_layer(bpy.context.scene.collection)
    bpy.context.view_layer.active_layer_collection = lay_coll
    bpy.ops.mesh.primitive_uv_sphere_add(
        segments=64,
        ring_count=32,
        enter_editmode=True,
        location=(0, 0, 0),
    )
    bpy.ops.uv.sphere_project()
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.shade_smooth()
    sphere = bpy.context.active_object
    files = append_particles(blendFile)
    for f in files:
        name = f['name']
        bpy.ops.object.particle_system_add()
        settings = bpy.data.particles[name]
        sphere.particle_systems[-1].settings = settings
        # for render preview
        settings.child_nbr = settings.rendered_child_count
    bpy.ops.object.select_all(action='DESELECT')
    # for ob in bpy.data.objects:
    #     if ob.type == 'CAMERA' or ob == sphere:
    #         pass
    #     else:
    #         ob.select_set(True)
    bpy.context.view_layer.objects.active = sphere
    bpy.data.collections['Assets'].hide_viewport = True
    sphere.select_set(True)
    sphere.particle_systems[-1].seed = 1


class SAM_OT_RenderPreviews(bpy.types.Operator):
    bl_idname = "simple_asset_manager.render_previews"
    bl_label = "(re)Render all previews"

    sub_process: BoolProperty()
    rerender: StringProperty()
    render_env: StringProperty()
    override_mats: StringProperty()
    matcap: StringProperty()

    def execute(self, context):
        bpy.ops.wm.save_userpref()
        pref = context.preferences.addons[__name__].preferences
        if self.rerender == '':
            rerender = pref.rerender
        else:
            rerender = True if self.rerender == 'True' else False
        if 'Assets' not in bpy.context.scene.collection.children.keys():
            asset_coll = bpy.data.collections.new('Assets')
            context.scene.collection.children.link(asset_coll)
        else:
            asset_coll = bpy.data.collections['Assets']
        # IF is for rendering in separate blender instance
        if not self.sub_process:
            # RESET CATEGORIES FOR IMAGE RELOAD
            context.scene.simple_asset_manager.local_global = "global"
            context.scene.simple_asset_manager.local_global = "local"
            context.scene.simple_asset_manager.local_global = "global"
            context.scene.simple_asset_manager_categories.cat0 = '.'
            pcoll = preview_collections["main"]
            pcoll.clear()

            lib_local = context.scene.simple_asset_manager.lib_local
            if platform().startswith('Windows'):
                lib_local = lib_local.replace('\\', '\\\\')
            command = [
                bpy.app.binary_path, '--no-window-focus', '--python-expr',
                'import bpy;'
                f'bpy.context.scene.simple_asset_manager.lib_local="{lib_local}";'
                'bpy.ops.simple_asset_manager.render_previews('
                f'sub_process=True, rerender="{str(rerender)}", '
                f'render_env="{str(pref.render_env)}",'
                f'override_mats="{str(pref.override_mats)}",'
                f'matcap="{str(pref.matcap)}");'
                'bpy.context.preferences.view.use_save_prompt=False;'
                'bpy.context.preferences.view.show_splash=False;'
                'bpy.ops.wm.quit_blender();'
            ]
            subprocess.Popen(command)
        else:
            lib_global = pref.lib_global
            lib_local = context.scene.simple_asset_manager.lib_local
            libs = [lib_global, lib_local] if lib_local else [lib_global]
            for lib_path in libs:
                for r, _, fs in os.walk(lib_path):
                    for f in fs:
                        render_type = 'RENDERED' if f.endswith(
                            ('.hdr', '.exr')) else 'MATERIAL'
                        if f.endswith(FORMATS):
                            blendFile = os.path.join(r, f)
                            print(blendFile)
                            png = os.path.splitext(blendFile)[0] + '.png'
                            if not rerender and os.path.exists(png):
                                continue
                            render = bpy.context.scene.render
                            render.filepath = os.path.splitext(blendFile)[0]
                            render.stamp_note_text = os.path.splitext(blendFile)[1][1:].upper()
                            prepare_scene()
                            setup_eevee()
                            with_cycles = False
                            cam = add_camera()
                            if f.endswith(('.hdr', '.exr')):
                                set_scene_hdr(blendFile, cam)
                                with_cycles = True
                            elif 'material' in r.lower():
                                set_scene_material(blendFile, cam)
                                if pref.cycles_material_previews:
                                    setup_cycles()
                                    with_cycles = True
                            elif 'particle' in r.lower():
                                set_scene_particle_settings(blendFile)
                            elif 'node' in r.lower():
                                pass
                            else:
                                active_layer = context.view_layer.active_layer_collection
                                append_element(blendFile, active_layer, link=False)
                            bpy.ops.object.select_all(action='SELECT')
                            if not f.endswith(('.hdr', '.exr')):
                                bpy.ops.view3d.camera_to_view_selected()
                                cam.data.lens = 40 if 'material' in r.lower() else 70
                            if f.endswith(('.hdr', '.exr')):
                                render_type = 'RENDERED'
                            elif 'material' in r.lower():
                                render_type = 'MATERIAL'
                            elif not bpy.data.materials or self.override_mats == 'True':
                                render_type = 'SOLID'
                            else:
                                render_type = 'MATERIAL'
                            for area in bpy.context.screen.areas:
                                area.type = 'VIEW_3D'
                                space = area.spaces[0]
                                space.region_3d.view_perspective = 'CAMERA'
                                space.shading.type = render_type
                                space.overlay.show_overlays = False
                                space.shading.light = 'STUDIO'
                                if render_type == 'MATERIAL':
                                    space.shading.studio_light = self.render_env
                                elif render_type == 'SOLID':
                                    space.shading.light = 'MATCAP'
                                    space.shading.color_type = 'SINGLE'
                                    space.shading.studio_light = self.matcap
                            if with_cycles:
                                bpy.ops.render.render(write_still=True)
                                # bpy.ops.render.render(write_still=True, use_viewport=True)
                            else:
                                bpy.ops.render.opengl(write_still=True)

        print("Done.")
        return {'FINISHED'}
