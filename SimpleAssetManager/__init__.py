# -*- coding: utf-8 -*-
import os
import subprocess

import bpy # type: ignore
from bpy.types import WindowManager # type: ignore
import bpy.utils.previews # type: ignore
from bpy.props import ( # type: ignore
    BoolProperty,
    PointerProperty,
    StringProperty,
    EnumProperty,
    FloatProperty,
)

bl_info = {
    "name": "Simple Asset Manager",
    "description": "Manager for objects, materials, particles, hdr.",
    "author": "Dawid Huczyński",
    "version": (1, 2, 1),
    "blender": (2, 80, 0),
    "location": "View 3D > Tool",
    "wiki_url": "https://gitlab.com/tibicen/simple-asset-manager",
    "tracker_url": "",
    "support": "COMMUNITY",
    "category": "Add Mesh",
}

# TODO: textures library


def developer():
    # AUTODEBUG OPTIONS: I KNOW IT IS LAME :P ...
    import platform
    import hashlib
    import logging

    h = platform.node() + platform.system() + platform.processor()
    h = hashlib.md5(h.encode())
    if h.hexdigest() == "813939f0614bad8533c309020488641b":
        # windows
        DEBUG = True
        DBG_LIB = "D:\\test_lib\\"
    elif h.hexdigest() == "6bd56b92d9a0dbe4d57802b2050e1ac5":
        DBG_LIB = "/home/tibicen/Inne/Code/test/"
        DEBUG = True
    else:
        DEBUG = False
        DBG_LIB = ""
    log = logging.getLogger(__name__)
    return DEBUG, DBG_LIB


DEBUG, DBG_LIB = developer()

# CONSTANTS
FORMATS = (".blend", ".obj", ".fbx", ".hdr", ".exr")
EXRS = (
    "city.exr",
    "courtyard.exr",
    "forest.exr",
    "interior.exr",
    "night.exr",
    "studio.exr",
    "sunrise.exr",
    "sunset.exr",
)
MATCAPS = (
    "basic_1.exr",
    "basic_2.exr",
    "basic_dark.exr",
    "basic_side.exr",
    "ceramic_dark.exr",
    "ceramic_lightbulb.exr",
    "check_normal+y.exr",
    "check_rim_dark.exr",
    "check_rim_light.exr",
    "clay_brown.exr",
    "clay_muddy.exr",
    "clay_studio.exr",
    "jade.exr",
    "metal_anisotropic.exr",
    "metal_carpaint.exr",
    "metal_lead.exr",
    "metal_shiny.exr",
    "pearl.exr",
    "reflection_check_horizontal.exr",
    "reflection_check_vertical.exr",
    "resin.exr",
    "skin.exr",
    "toon.exr",
)

from .helpers import (
    get_library_path,
    scan_for_elements,
    get_assets_coll,
    append_element,
    find_layer,
    append_hdr,
    append_material,
    append_particles,
    rotate_hdr,
    execute_insert,
    gen_thumbnails,
)


def update_folder(self, context, name):
    manager = context.scene.simple_asset_manager
    a = getattr(self, name)
    for k, v in BTN_TABLE.items():
        if v.endswith(a):
            context.scene["simple_asset_manager_categories"][k] = 0
    if a == ".":
        manager.curr_folder = BTN_TABLE[name]
    else:
        manager.curr_folder = os.path.join(BTN_TABLE[name], a)


def update_folder_wrapper(name):
    def update(self, context):
        return update_folder(self, context, name)

    return update


def update_cat_buttons(self, context):
    pass


def print_folder(self, context):
    print("Current folder:", self.curr_folder)


def update_lib(self, context):
    if context.scene.simple_asset_manager.local_global == "local":
        lib_path = context.scene.simple_asset_manager.lib_local
    else:
        lib_path = context.preferences.addons[__name__].preferences.lib_global

    context.scene.simple_asset_manager.curr_folder = lib_path

    global BUTTONS
    global BTN_TABLE
    BUTTONS = {}
    BTN_TABLE = {}
    nr = 0
    for r, dirs, fs in os.walk(lib_path):
        k = r.split(lib_path)[1]
        items = [(x, x, "", n + 1) for n, x in enumerate(sorted(dirs))]
        if items:
            items.insert(0, (".", ".", "", 0))
            BUTTONS[f"cat{nr}"] = items
            BTN_TABLE[f"cat{nr}"] = r.rstrip(os.path.sep)
            nr += 1
    attributes = {}
    for i, items in BUTTONS.items():
        name = BTN_TABLE[i].split(os.path.sep)[-1]
        attributes[i] = EnumProperty(
            items=items, name=name, update=update_folder_wrapper(i)
        )

    dynamic_cats = type(
        "simple_asset_manager_categories", (bpy.types.PropertyGroup,), {'__annotations__': attributes}
    )

    # TODO: Check if below is still necesarry.
    try:
        bpy.utils.unregister_class(
            bpy.types.Scene.simple_asset_manager_categories[1]["type"]
        )
    except:
        bpy.utils.unregister_class(
            bpy.types.Scene.simple_asset_manager_categories.keywords['type']
        )

    bpy.utils.register_class(dynamic_cats)
    bpy.types.Scene.simple_asset_manager_categories = PointerProperty(type=dynamic_cats)


def enum_previews_from_directory_items(self, context, folder_name):
    # GET THE PREVIEW COLLECTION (DEFINED IN REGISTER FUNC)
    pcoll = preview_collections["main"]
    manager = context.scene.simple_asset_manager
    directory = get_library_path()
    directory = manager.curr_folder

    empty_path = os.path.join(os.path.dirname(__file__), "empty.png")
    enum_items = []

    # ENUMPROPERTY CALLBACK
    if context is None:
        return enum_items
    # wm = context.window_manager
    if directory == pcoll.simple_asset_manager_prev_dir:
        return pcoll.simple_asset_manager_prevs
    # print("Simple Asset Manager - Scanning directory: %s" % directory)
    if directory and os.path.exists(directory):
        image_paths = []
        for fn in os.listdir(directory):
            prev = scan_for_elements(os.path.join(directory, fn))
            if prev:
                image_paths.append(prev)

        enum_items = gen_thumbnails(image_paths, enum_items, pcoll, empty_path)
    # RETURN VALIDATION
    if not enum_items:
        if "empty" in pcoll:
            enum_items.append(("empty", "", "", pcoll["empty"].icon_id, 0))
        else:
            empty = pcoll.load("empty", empty_path, "IMAGE")
            enum_items.append(("empty", "", "", empty.icon_id, 0))
    pcoll.simple_asset_manager_prevs = enum_items
    pcoll.simple_asset_manager_prev_dir = directory
    bpy.data.window_managers[0]["simple_asset_manager_prevs"] = 0
    return pcoll.simple_asset_manager_prevs


def update_previews_wrapper(category_name):
    def update(self, context):
        enum = enum_previews_from_directory_items(self, context, category_name)
        if category_name is None:
            return enum

    return update


def search_library(self, context):
    pref = context.preferences.addons[__name__].preferences
    lib_path = get_library_path()
    empty_path = os.path.join(os.path.dirname(__file__), "empty.png")
    pcoll = preview_collections["main"]
    items = []
    enum_items = []
    for r, _, files in os.walk(lib_path):
        for file in sorted(files):
            prev = scan_for_elements(os.path.join(r, file))
            if prev:
                items.append(prev)
    enum_items = gen_thumbnails(items, enum_items, pcoll, empty_path)
    if not enum_items:
        if "empty" in pcoll:
            enum_items.append(("empty", "", "", pcoll["empty"].icon_id, 0))
        else:
            empty = pcoll.load("empty", empty_path, "IMAGE")
            enum_items.append(("empty", "", "", empty.icon_id, 0))
    return enum_items


class SAM_categories(bpy.types.PropertyGroup):
    global BUTTONS
    global BTN_TABLE
    BUTTONS = {}
    BTN_TABLE = {}


class SimpleAssetManager(bpy.types.PropertyGroup):
    origin: BoolProperty(name="Origin", description="Placement location")

    incl_cursor_rot: BoolProperty(
        name="Rotation", description="Includes cursor rotation on import."
    )

    hdr_rot: FloatProperty(
        name="Rotation",
        default=0.0,
        subtype="ANGLE",
        description="Rotates active world HDR image",
        min=-360,
        max=360,
        update=rotate_hdr,
    )

    lib_local: StringProperty(
        name="Local/Project Library Path",
        default="",
        description="SAM uses this path if not empty, otherwise uses master library from preferences.",
        subtype="DIR_PATH",
        update=update_lib,
    )

    curr_folder: StringProperty(
        name="Local/Project Library Path",
        default="",
        description="SAM uses this path if not empty, otherwise uses master library from preferences.",
        subtype="DIR_PATH",
        # update=print_folder,
    )

    local_global: EnumProperty(
        items=[("global", "global", "", 0), ("local", "local", "", 1)],
        default="global",
        update=update_lib,
    )


class SAM_OT_Search(bpy.types.Operator):
    """Acces to your Objects Library"""

    bl_idname = "simple_asset_manager.search_dynamic"
    bl_label = "Search library"
    bl_options = {"REGISTER", "UNDO"}
    bl_property = "items"

    items: EnumProperty(items=search_library)

    def invoke(self, context, event):
        context.window_manager.invoke_search_popup(self)
        return {"FINISHED"}

    def execute(self, context):
        lib_path = get_library_path()
        manager = context.scene.simple_asset_manager_categories
        path = os.path.dirname(self.items)

        manager.curr_folder = path

        newp = path
        commands = []
        while newp + os.path.sep != lib_path:
            newp, folder = newp.rsplit(os.path.sep, 1)
            inv_table = {v: k for k, v in BTN_TABLE.items()}
            commands.append((inv_table[newp], folder))

        for k, v in reversed(commands):
            setattr(manager, k, v)

        bpy.data.window_managers[0].simple_asset_manager_prevs = self.items
        return {"FINISHED"}


class SAM_OT_LinkButton(bpy.types.Operator):
    bl_idname = "simple_asset_manager.link_object"
    bl_label = "Collection"
    bl_description = "Link object as collection"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        execute_insert(context, link=True)
        return {"FINISHED"}


class SAM_OT_AppendButton(bpy.types.Operator):
    bl_idname = "simple_asset_manager.append_object"
    bl_label = "Append"
    bl_description = "Appends object to scene"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        execute_insert(context, link=False)
        return {"FINISHED"}


class SAM_OT_AppendMaterialButton(bpy.types.Operator):
    bl_idname = "simple_asset_manager.append_material"
    bl_label = "Append"
    bl_description = "Adds material to blendfile"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        execute_insert(context, link=False)
        return {"FINISHED"}


class SAM_OT_AddMaterialButton(bpy.types.Operator):
    bl_idname = "simple_asset_manager.add_material"
    bl_label = "Add"
    bl_description = "Adds material to object"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        active_ob = context.active_object
        wm = bpy.data.window_managers["WinMan"]
        for ob in bpy.context.scene.objects:
            ob.select_set(False)
        bpy.ops.object.select_all(action="DESELECT")
        selected_preview = wm.simple_asset_manager_prevs
        files = append_material(selected_preview)
        for file in files:
            mat = bpy.data.materials[file["name"]]
            active_ob.data.materials.append(mat)
        active_ob.select_set(True)
        return {"FINISHED"}


class SAM_OT_ReplaceMaterialButton(bpy.types.Operator):
    bl_idname = "simple_asset_manager.replace_material"
    bl_label = "Replace"
    bl_description = "Replace objects material"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        active_ob = context.active_object
        wm = bpy.data.window_managers["WinMan"]
        for ob in bpy.context.scene.objects:
            ob.select_set(False)
        bpy.ops.object.select_all(action="DESELECT")
        selected_preview = wm.simple_asset_manager_prevs
        files = append_material(selected_preview)
        for file in files:
            mat = bpy.data.materials[file["name"]]
            active_ob.data.materials[active_ob.active_material_index] = mat
        active_ob.select_set(True)
        return {"FINISHED"}


class SAM_OT_AddParticlesButton(bpy.types.Operator):
    bl_idname = "simple_asset_manager.add_particles"
    bl_label = "Add to object"
    bl_description = "Adds particles to object"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        active_ob = context.active_object
        bpy.ops.object.select_all(action="DESELECT")
        files = execute_insert(context, link=False)
        active_ob.select_set(True)
        for file in files:
            bpy.ops.object.particle_system_add()
            par_sys = active_ob.particle_systems[-1]
            par_sys.settings = bpy.data.particles[file["name"]]
            par_sys.name = file["name"]
        return {"FINISHED"}


class SAM_OT_OpenButton(bpy.types.Operator):
    bl_idname = "simple_asset_manager.open_file"
    bl_label = "Open File"

    def execute(self, context):
        addon_prefs = context.preferences.addons[__name__].preferences
        wm = bpy.data.window_managers["WinMan"]
        if addon_prefs.opensame:
            selected_preview = wm.simple_asset_manager_prevs
            bpy.ops.wm.open_mainfile(filepath=selected_preview)
        else:
            selected_preview = wm.simple_asset_manager_prevs
            command = [bpy.app.binary_path, selected_preview]
            subprocess.Popen(command)
        return {"FINISHED"}


class SAM_OT_LoadFolders(bpy.types.Operator):
    bl_idname = "simple_asset_manager.load_folders"
    bl_label = "Load Library"
    bl_description = "Need this to load library folders structure for the first time"
    bl_options = {"REGISTER"}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        context.scene.simple_asset_manager.local_global = "global"
        context.scene.simple_asset_manager.local_global = "local"
        context.scene.simple_asset_manager.local_global = "global"
        context.scene.simple_asset_manager_categories.cat0 = "."
        return {"FINISHED"}


def SAM_UI(self, context):
    layout = self.layout
    wm = context.window_manager
    manager = context.scene.simple_asset_manager
    curr_folder = manager.curr_folder
    categories = context.scene.simple_asset_manager_categories
    pref = context.preferences.addons[__name__].preferences
    col = layout.column()
    if pref.lib_global == "" or not os.path.exists(pref.lib_global):
        col.label(text="Please fill existing")
        col.label(text="library path in preferences")
        return 0

    btns = list(BUTTONS.keys())
    col = layout.column()
    if not btns:
        col.operator("simple_asset_manager.load_folders")
    else:
        if manager.lib_local != "":
            row = col.row()
            row.prop(manager, "local_global", expand=True)

        # Search bar
        col.operator("simple_asset_manager.search_dynamic", icon="VIEWZOOM")
        # Categories Drop Down Menu
        col = layout.column(align=True)
        col.prop(categories, btns.pop(0), text='')
        for btn in btns:
            if BTN_TABLE[btn] in curr_folder:
                col.prop(categories, btn, text='')

        # Previews
        if wm.simple_asset_manager_prevs not in ("", "empty"):
            # row = layout.row()
            if wm.simple_asset_manager_prevs != "empty":
                col.template_icon_view(
                    wm,
                    "simple_asset_manager_prevs",
                    show_labels=True,
                    scale=pref.prev_size,
                    scale_popup=pref.prev_scale,
                )
                ## Would be nice to have it but hard to implement.
                ## template_ID_preview depends on blender id which is assigned after importing.
                ## Addon would need to import all library elements to use this one.
                # col.template_ID_preview(
                #     wm,
                #     "simple_asset_manager_prevs",
                #     new="",
                #     open="",
                #     unlink="",
                #     rows=5,
                #     cols=5,
                #     filter='ALL',
                #     hide_buttons=False)
                name = os.path.splitext(
                    os.path.basename(wm.simple_asset_manager_prevs)
                )[0]
                name = name.replace("_", " ").replace(".", " ").capitalize()
                col.label(text=name)
            # Materials
            path = os.path.dirname(wm.simple_asset_manager_prevs).lower()
            material = "material" in path
            particle = "particle" in path
            hdr = wm.simple_asset_manager_prevs.endswith((".hdr", ".hdri", ".exr"))
            if material:
                col.operator("simple_asset_manager.append_material")
                if context.active_object.type not in ("EMPTY", "LIGHT"):
                    col.operator("simple_asset_manager.add_material")
                    if context.active_object.material_slots:
                        col.operator("simple_asset_manager.replace_material")
                row = col.row(align=True)
                row.operator("simple_asset_manager.link_object")
                row.operator("simple_asset_manager.open_file")
            elif particle:
                col.operator("simple_asset_manager.add_particles")
                col.operator("simple_asset_manager.link_object")
                row = col.row(align=True)
                row.operator("simple_asset_manager.append_object")
                row.operator("simple_asset_manager.open_file")
            elif hdr:
                col.operator("simple_asset_manager.append_object")
                col = layout.column()
                nodes = context.scene.world.node_tree.nodes
                if "Background" in nodes.keys():
                    strength = nodes["Background"].inputs[1]
                    col.prop(strength, "default_value", text="Strength")
                if "Mapping" in nodes.keys():
                    col.prop(manager, "hdr_rot")
            else:  # blend, obj, fbx
                if wm.simple_asset_manager_prevs.endswith(".blend"):
                    col.operator("simple_asset_manager.link_object")

                row = col.row(align=True)
                row.operator("simple_asset_manager.append_object")
                if wm.simple_asset_manager_prevs.endswith(".blend"):
                    row.operator("simple_asset_manager.open_file")
            if not material and not particle and not hdr:
                origin_btn_name = "At Origin"
                if manager.origin:
                    origin_btn_name = "At Cursor"
                row = layout.row(align=True)
                row.prop(manager, "origin", text=origin_btn_name, toggle=True)

                if manager.origin:
                    row.prop(manager, "incl_cursor_rot")
                else:
                    row.label(text="")


global preview_collections
preview_collections = {}

from .previews import SAM_OT_RenderPreviews
from .ui import (
    SAM_PT_Panel,
    SAM_PT_Popup,
    SAM_PT_PrefPanel,
    SAM_MT_button,
    SAM_MT_Library_Path,
)


#####################################################################
# REGISTER

classes = (
    SAM_PT_PrefPanel,
    SAM_OT_LinkButton,
    SAM_OT_AppendButton,
    SAM_OT_AppendMaterialButton,
    SAM_OT_AddMaterialButton,
    SAM_OT_ReplaceMaterialButton,
    SAM_OT_AddParticlesButton,
    SAM_OT_OpenButton,
    SAM_OT_Search,
    SimpleAssetManager,
    SAM_PT_Panel,
    SAM_PT_Popup,
    SAM_OT_RenderPreviews,
    SAM_OT_LoadFolders,
    SAM_categories,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    WindowManager.simple_asset_manager_prev_dir = StringProperty(
        name="Folder Path", subtype="DIR_PATH", default=""
    )

    WindowManager.simple_asset_manager_prevs = EnumProperty(
        items=update_previews_wrapper(None)
    )

    pcoll = bpy.utils.previews.new()
    pcoll.simple_asset_manager_prev_dir = ""
    pcoll.simple_asset_manager_prevs = ""

    preview_collections["main"] = pcoll
    bpy.types.Scene.simple_asset_manager = PointerProperty(type=SimpleAssetManager)

    bpy.types.Scene.simple_asset_manager_categories = PointerProperty(
        type=SAM_categories
    )

    bpy.types.VIEW3D_MT_add.append(SAM_MT_button)
    bpy.types.SCENE_PT_scene.append(SAM_MT_Library_Path)


# UNREGISTER
def unregister():
    bpy.types.VIEW3D_MT_add.remove(SAM_MT_button)
    bpy.types.SCENE_PT_scene.remove(SAM_MT_Library_Path)
    del WindowManager.simple_asset_manager_prevs

    for pcoll in preview_collections.values():
        bpy.utils.previews.remove(pcoll)
    preview_collections.clear()

    for cls in reversed(classes[:-1]):  # Drops SAM_categories
        bpy.utils.unregister_class(cls)

    try:
        bpy.utils.unregister_class(
            bpy.types.Scene.simple_asset_manager_categories[1]["type"]
        )
    except:
        bpy.utils.unregister_class(
            bpy.types.Scene.simple_asset_manager_categories.keywords['type']
        )
 

    del bpy.types.Scene.simple_asset_manager
    del bpy.types.Scene.simple_asset_manager_categories


if __name__ == "__main__":
    register()
