import bpy # type: ignore
import os

from . import FORMATS, __name__


def get_library_path():
    lib_global = bpy.context.preferences.addons[__name__].preferences.lib_global
    lib_local = bpy.context.scene.simple_asset_manager.lib_local
    lg = bpy.context.scene.simple_asset_manager.local_global
    lib_path = lib_global if lg == "global" else lib_local
    return bpy.path.abspath(lib_path)


def scan_for_elements(path):
    if path.lower().endswith(FORMATS):
        png = os.path.splitext(path)[0] + ".png"
        if os.path.exists(png):
            return (path, True)
        else:
            return (path, False)
    else:
        return None


def get_assets_coll():
    if "Assets" not in bpy.context.scene.collection.children.keys():
        asset_coll = bpy.data.collections.new("Assets")
        bpy.context.scene.collection.children.link(asset_coll)
        bpy.context.view_layer.layer_collection.children["Assets"].exclude = True
    else:
        asset_coll = bpy.data.collections["Assets"]
    return asset_coll


def find_layer(coll, lay_coll=None):
    if lay_coll is None:
        lay_coll = bpy.context.view_layer.layer_collection
    if lay_coll.collection == coll:
        return lay_coll
    else:
        for child in lay_coll.children:
            a = find_layer(coll, child)
            if a:
                return a
        return None


def rotate_hdr(self, context):
    nodes = context.scene.world.node_tree.nodes
    if "Mapping" in nodes.keys():
        if bpy.app.version < (2, 81):
            nodes["Mapping"].rotation[2] = self.hdr_rot
        else:
            nodes["Mapping"].inputs[2].default_value[2] = self.hdr_rot


def append_blend_objects(blendFile, link, active_layer, coll_name):
    if link:
        asset_coll = get_assets_coll()
        if coll_name in bpy.data.collections.keys():
            # IF EXISTS THEN INSTANCE IT END FINISH
            # FIXED: on second link it links twice !!! Was duplicated on append_element last line
            # bpy.ops.object.collection_instance_add(collection=coll_name)
            return True
        else:
            obj_coll = bpy.data.collections.new(coll_name)
            asset_coll.children.link(obj_coll)
            obj_lay_coll = find_layer(obj_coll)
            bpy.context.view_layer.active_layer_collection = obj_lay_coll
    else:
        obj_coll = active_layer.collection

    objects = []
    scenes = []
    with bpy.data.libraries.load(blendFile) as (data_from, _):
        for name in data_from.scenes:
            scenes.append({"name": name})
    action = bpy.ops.wm.link if link else bpy.ops.wm.append
    if bpy.app.version >= (2, 90, 0):
        action(
            directory=blendFile + "/Scene/",
            files=scenes,
            instance_collections=False,
            instance_object_data=False,
            link=False,
        )
        for lib in bpy.data.libraries:
            if lib.filepath == blendFile:
                bpy.data.libraries.remove(lib)
    else:
        action(
            directory=blendFile + "/Scene/",
            files=scenes,
            instance_collections=False,
            link=False,
        )

    scenes = bpy.data.scenes[-len(scenes):]
    target_coll = obj_coll if link else active_layer.collection
    for scene in scenes:
        scene.name = "[SAM]: " + coll_name
        objs = 0
        for ob in scene.collection.objects:
            try:
                target_coll.objects.link(ob)  # MASTER COLLECTION
            except RuntimeError as e:
                if e.args[0].endswith("already in collection 'Collection'\n"):
                    pass
                else:
                    raise e
            objs += 1
            objects.append(ob)
        for coll in scene.collection.children:
            if coll.name.startswith("Collection"):
                for ob in coll.objects:
                    target_coll.objects.link(ob)
                    objects.append(ob)
                for sub_coll in coll.children:
                    target_coll.children.link(sub_coll)
            else:
                target_coll.children.link(coll)
        bpy.data.scenes.remove(scene, do_unlink=True)
    sel = False if link else True
    for obj in objects:
        obj.select_set(sel)

    return objects


def append_blend_collections(blendFile, link):
    collections = []
    with bpy.data.libraries.load(blendFile) as (data_from, _):
        for name in data_from.collections:
            collections.append({"name": name})
    action = bpy.ops.wm.link if link else bpy.ops.wm.append
    action(directory=blendFile + "/Collection/", files=collections)
    # SEARCH FOR NESTED COLLECTIONS
    for coll in collections:
        keys = bpy.data.collections[coll["name"]].children.keys()
        for k in keys:
            pass


def append_element(blendFile, active_layer, link=False):
    # CREATE ASSETS COLLECTIONS
    coll_name = os.path.splitext(os.path.basename(blendFile))[0].title()

    # IMPORT OBJECTS
    existing_objs = bpy.data.objects.values()
    objects = []
    # OBJECTS
    if blendFile.endswith(".obj"):
        bpy.ops.import_scene.obj(filepath=blendFile)
        for ob in bpy.context.selected_objects:
            objects.append(ob)
    elif blendFile.endswith(".fbx"):
        bpy.ops.import_scene.fbx(filepath=blendFile)
        for ob in bpy.context.selected_objects:
            objects.append(ob)
    elif blendFile.endswith(".blend"):
        objects = append_blend_objects(blendFile, link, active_layer, coll_name)
        # append_blend_collections(blendFile, link)

    # # COPY DATA FROM PREVIOUS IMPORT IF EXISTS
    # for ob in objects:
    #     if "." in ob.name and ob.name.rsplit(".")[-1].isdigit():
    #         for eob in existing_objs:
    #             if (
    #                 ob.name.rsplit(".", 1)[0] == eob.name.rsplit(".", 1)[0]
    #                 and ob.data.name.rsplit(".", 1)[0]
    #                 == eob.data.name.rsplit(".", 1)[0]
    #                 and "." in ob.data.name
    #                 and ob.data.name.rsplit(".")[-1].isdigit()
    #             ):
    #                 ob.data = eob.data
    bpy.context.view_layer.active_layer_collection = active_layer
    if link:
        bpy.ops.object.collection_instance_add(collection=coll_name)


def append_hdr(path):
    file = os.path.basename(path)
    # CHECK IF ALREADY LOADED
    if file in bpy.data.worlds.keys():
        world = bpy.data.worlds[file]
    else:
        bpy.ops.image.open(filepath=path)
        im = bpy.data.images[file]
        world = bpy.data.worlds.new(file)
        world.use_nodes = True
        nodes = world.node_tree.nodes
        tex = nodes.new("ShaderNodeTexEnvironment")
        tex.image = im
        mapping = nodes.new("ShaderNodeMapping")
        coord = nodes.new("ShaderNodeTexCoord")
        background = nodes["Background"]
        tex.location = (-300, 300)
        mapping.location = (-700, 300)
        coord.location = (-900, 300)
        world.node_tree.links.new(background.inputs[0], tex.outputs[0])
        world.node_tree.links.new(tex.inputs[0], mapping.outputs[0])
        world.node_tree.links.new(mapping.inputs[0], coord.outputs[0])
    bpy.context.scene.world = world


def append_material(blendFile, link=False):
    files = []
    with bpy.data.libraries.load(blendFile) as (data_from, _):
        for name in data_from.materials:
            files.append({"name": name})
    action = bpy.ops.wm.link if link else bpy.ops.wm.append
    for file in files:
        if file["name"] not in bpy.data.materials.keys():
            action(directory=blendFile + "/Material/", files=[file,])
    return files


def append_particles(blendFile, link=False):
    particles = []
    asset_coll = get_assets_coll()
    if "Particles" not in bpy.data.collections.keys():
        particles_coll = bpy.data.collections.new("Particles")
        asset_coll.children.link(particles_coll)
    else:
        particles_coll = bpy.data.collections["Particles"]
    with bpy.data.libraries.load(blendFile) as (data_from, _):
        for name in data_from.particles:
            particles.append({"name": name})
    exists = True
    for name in [x["name"] for x in particles]:
        if name not in bpy.data.particles.keys():
            exists = False
    if exists:
        return particles
    coll_name = os.path.splitext(os.path.basename(blendFile))[0].title()
    obj_coll = bpy.data.collections.new(coll_name)
    particles_coll.children.link(obj_coll)
    obj_lay_coll = find_layer(obj_coll)
    bpy.context.view_layer.active_layer_collection = obj_lay_coll
    action = bpy.ops.wm.link if link else bpy.ops.wm.append
    # colls = bpy.data.collections[:]
    action(directory=blendFile + "/ParticleSettings/", files=particles)
    return particles


def execute_insert(context, link):
    active_layer = context.view_layer.active_layer_collection
    for ob in bpy.context.scene.objects:
        ob.select_set(False)
    bpy.ops.object.select_all(action="DESELECT")
    selected_preview = bpy.data.window_managers["WinMan"].simple_asset_manager_prevs
    folder = os.path.split(os.path.split(selected_preview)[0])[1]
    # APPEND OBJECTS
    if "material" in folder.lower():
        return append_material(selected_preview, link)
    elif "particle" in folder.lower():
        files = append_particles(selected_preview, link)
        return files
    elif selected_preview.endswith((".hdr", ".exr")):
        append_hdr(selected_preview)
    else:
        curr_pivot = context.scene.tool_settings.transform_pivot_point
        context.scene.tool_settings.transform_pivot_point = "CURSOR"
        cur = context.scene.cursor
        cur_loc = cur.location.copy()
        cur_rot = cur.rotation_euler.copy()
        cur.location = (0, 0, 0)
        cur.rotation_euler = (0, 0, 0)
        append_element(selected_preview, active_layer, link)
        if context.scene.simple_asset_manager.origin:
            if context.scene.simple_asset_manager.incl_cursor_rot:
                for x, y in list(zip(cur_rot, "XYZ")):
                    bpy.ops.transform.rotate(value=-x, orient_axis=y)
                bpy.ops.transform.translate(value=cur_loc)
                # bpy.ops.wm.tool_set_by_id(name="builtin.rotate")
            else:
                cur.location = cur_loc
                cur.rotation_euler = cur_rot
                cur_loc = context.scene.cursor.location
                bpy.ops.transform.translate(value=cur_loc)
        cur.location = cur_loc
        cur.rotation_euler = cur_rot
        context.scene.tool_settings.transform_pivot_point = curr_pivot
    context.view_layer.active_layer_collection = active_layer


# UI


def gen_thumbnails(image_paths, enum_items, pcoll, empty_path):
    # FOR EACH IMAGE IN THE DIRECTORY, LOAD THE THUMB
    # UNLESS IT HAS ALREADY BEEN LOADED
    for i, im in enumerate(sorted(image_paths)):
        filepath, prev = im
        name = os.path.splitext(os.path.basename(filepath))[0]
        if name.startswith("._"):
            continue
        name = name.replace(".", " ").replace("_", " ").lower().capitalize()
        if filepath in pcoll:
            enum_items.append((filepath, name, "", pcoll[filepath].icon_id, i))
        else:
            if prev:
                imgpath = filepath.rsplit(".", 1)[0] + ".png"
                # if filepath.endswith(('.hdr', '.exr')):
                #     imgpath = filepath
                thumb = pcoll.load(filepath, imgpath, "IMAGE")
            else:
                thumb = pcoll.load(filepath, empty_path, "IMAGE")
            enum_items.append((filepath, name, "", thumb.icon_id, i))
    return enum_items
