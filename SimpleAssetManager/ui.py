import os
import bpy  # type: ignore
from bpy.props import BoolProperty, StringProperty, EnumProperty, FloatProperty  # type: ignore

from . import DEBUG, DBG_LIB, EXRS, MATCAPS, SAM_UI, update_lib, __name__


class SAM_PT_Panel(bpy.types.Panel):
    """Create a Panel in the Tool Shelf"""

    bl_label = "Simple Asset Manager"
    bl_idname = "IMPORT_PT_Asset_Manager"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Tool"
    bl_options = {"DEFAULT_CLOSED"}

    # Draw
    def draw(self, context):
        SAM_UI(self, context)


class SAM_PT_Popup(bpy.types.Operator):
    """Acces to your Objects Library"""

    bl_idname = "view3d.add_asset"
    bl_label = "Simple Asset Manager"
    bl_options = {"REGISTER", "UNDO"}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        SAM_UI(self, context)

    def execute(self, context):
        return {"FINISHED"}


class SAM_PT_PrefPanel(bpy.types.AddonPreferences):
    bl_idname = __name__
    lib_global: StringProperty(
        name="Library Path",
        default=DBG_LIB if DEBUG else os.path.splitdrive(__file__)[0],
        description="Show only hotkeys that have this text in their name",
        subtype="DIR_PATH",
        update=update_lib,
    )

    rerender: BoolProperty(default=False)

    opensame: BoolProperty(default=False)

    cycles_material_previews: BoolProperty(default=False)

    override_mats: BoolProperty(default=False)

    incl_cursor_rot: BoolProperty(
        name="Rotation", description="Include rotation when appending to cursor."
    )

    exrs = [
        (exr, exr.replace(".exr", "").title(), "", nr) for nr, exr in enumerate(EXRS)
    ]
    matcaps = [
        (matcap, matcap.replace(".exr", "").title(), "", nr)
        for nr, matcap in enumerate(MATCAPS)
    ]
    render_env: EnumProperty(
        items=exrs,
        name="Render Scene",
        description="With what light render the previews.",
        default="interior.exr",
    )
    matcap: EnumProperty(
        items=matcaps,
        name="Render Scene",
        description="When no material in file render with this matcap.",
        default="pearl.exr",
    )

    prev_scale: FloatProperty(
        name="Displayed preview images size",
        description="Lower value gives more elements in popup window.",
        default=4.0,
    )

    prev_size: FloatProperty(
        name="Displayed previews area size",
        description="Higer values gives biger popup window.",
        default=6.0,
    )

    def draw(self, context):
        manager = context.scene.simple_asset_manager
        layout = self.layout
        layout = layout.column_flow(columns=2, align=True)
        col = layout.column()
        col.prop(self, "lib_global", text="Master Library path")
        col.prop(manager, "lib_local", text="Project Library path")
        col.prop(self, "prev_scale")
        # col.prop(self, "prev_size")
        col.prop(self, "opensame", text="Open file on the same instance")
        row = layout.row()
        row.label(text="")
        row = layout.row()
        row.label(text="")
        row = layout.row()
        row.label(text="Previews generation:")
        row = layout.row()
        row.operator(
            "simple_asset_manager.render_previews", text="Render missing previews"
        )
        row.prop(self, "rerender", text="Re-render ALL previews")
        row = layout.row()
        row.label(text="")
        row.prop(self, "override_mats", text="Override all materials with matcap")
        row = layout.row()
        row.label(text="")
        row.prop(
            self, "cycles_material_previews", text="Use Cycles for Material Previews"
        )

        row = layout.row()
        row.prop(self, "render_env", text="Studio environment")
        row = layout.row()
        row.prop(self, "matcap", text="Matcap")


def SAM_MT_button(self, context):
    self.layout.operator(SAM_PT_Popup.bl_idname, text="Simple Asset Manager")


def SAM_MT_Library_Path(self, context):
    asset = context.scene.simple_asset_manager
    layout = self.layout
    layout.label(text="Simple Asset Manager:")
    layout.prop(asset, "lib_local")
