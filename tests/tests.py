import os
import sys
import unittest
import random
from shutil import copytree, rmtree
from math import pi
import bpy  # type: ignore
from mathutils import Vector, Euler  # type: ignore
import addon_utils  # type: ignore

bpy.context.preferences.view.use_save_prompt = False
bpy.context.preferences.view.show_splash = False

PATH = os.path.join(os.getcwd(), "library")

FOLDERS = [
    "obiekty",
    "obiekty samochody",
    "obiekty malpy struny konie woly psy kosci gorne dziewiec dziesiec",
    "fbx",
    "obj",
    "materialy",
    "hdr",
    "particle",
]


def euler_difference(E1, E2):
    dx = abs(E1.x - E2.x)
    dy = abs(E1.y - E2.y)
    dz = abs(E1.z - E2.z)
    return (dx, dy, dz)


def cleanup():
    for ob in bpy.data.objects:
        ob.hide_select = False
        ob.hide_render = False
        ob.hide_viewport = False
        ob.hide_set(False)
        bpy.data.objects.remove(ob)
    for coll in bpy.data.collections:
        coll.hide_select = False
        coll.hide_render = False
        coll.hide_viewport = False
    purge(bpy.data.collections)
    purge(bpy.data.objects)
    purge(bpy.data.cameras)
    purge(bpy.data.lights)
    purge(bpy.data.meshes)
    purge(bpy.data.particles)
    purge(bpy.data.materials)
    purge(bpy.data.textures)
    purge(bpy.data.images)
    purge(bpy.data.collections)


def random_material(name):
    mat = bpy.data.materials.new(name)
    mat.use_nodes = True
    BSDF = mat.node_tree.nodes["Principled BSDF"].inputs
    BSDF["Base Color"].default_value = (
        random.random(),
        random.random(),
        random.random(),
        1,
    )
    BSDF["Roughness"].default_value = random.random()
    BSDF["Specular"].default_value = random.random()
    BSDF["Clearcoat"].default_value = random.random()
    BSDF["Clearcoat Roughness"].default_value = random.random()
    return mat


def override_context():
    for window in bpy.context.window_manager.windows:
        screen = window.screen
        for area in screen.areas:
            if area.type == "VIEW_3D":
                override = {"window": window, "screen": screen, "area": area}
                bpy.ops.screen.screen_full_area(override)
                break


def clean_previews(path):
    for r, _, files in os.walk(path):
        for f in files:
            if f.endswith(".png"):
                os.remove(os.path.join(r, f))


def append_test(self, fp):
    cleanup()
    _ , ext = os.path.splitext(fp)
    sam = bpy.context.scene.simple_asset_manager
    sam.incl_cursor_rot = False
    sam.origin = False
    # import
    bpy.ops.simple_asset_manager.append_object()
    ob = bpy.data.objects[0]
    self.assertEqual(len(bpy.data.objects), 1)
    self.assertEqual(ob.location, Vector((0, 0, 0)))
    x = pi / 2 if ext == ".obj" else 0
    delta = euler_difference(ob.rotation_euler, Euler((x, 0, 0), "XYZ"))
    for n in delta:
        self.assertLess(n, 0.0001)

    # import + cur
    cleanup()
    sam.origin = True
    bpy.ops.simple_asset_manager.append_object()
    ob = bpy.data.objects[0]
    self.assertEqual(len(bpy.data.objects), 1)
    loc = bpy.context.scene.cursor.location
    self.assertEqual(ob.location, loc)
    delta = euler_difference(ob.rotation_euler, Euler((x, 0, 0), "XYZ"))
    for n in delta:
        self.assertLess(n, 0.0001)

    # TODO: fix wrong context "helpers.py", line 274, in execute_insert
    # # import + cur + rot
    # sam.incl_cursor_rot = True
    # cleanup()
    # bpy.ops.simple_asset_manager.append_object()
    # ob = bpy.data.objects[0]
    # self.assertEqual(len(bpy.data.objects), 1)
    # loc = bpy.context.scene.cursor.location
    # self.assertEqual(ob.location, loc)
    # cur_e = bpy.context.scene.cursor.rotation_euler
    # if ext == ".obj":
    #     e = Euler((cur_e.x + pi / 2, cur_e.y, cur_e.z))
    # else:
    #     e = cur_e
    # delta = euler_difference(ob.rotation_euler, e)
    # for n in delta:
    #     self.assertLess(n, 0.0001)


def select_asset(path, file):
    scene = bpy.context.scene
    bpy.ops.simple_asset_manager.load_folders()
    folders = path.replace(PATH, "").lstrip(os.path.sep).split(os.path.sep)
    scene.simple_asset_manager.local_global = "local"
    scene.simple_asset_manager.local_global = "global"
    for nr, element in enumerate(folders):
        cats = scene.simple_asset_manager_categories
        setattr(cats, f"cat{nr}", element)

    bpy.context.window_manager.simple_asset_manager_prevs = os.path.join(path, file)


class SAM_Testing(unittest.TestCase):
    def setUp(self):
        # clean scene:
        C = bpy.context
        cleanup()
        loaded = addon_utils.check("SimpleAssetManager")
        self.assertEqual(loaded[1], True)
        # setup library path
        C.preferences.addons["SimpleAssetManager"].preferences["lib_global"] = PATH
        bpy.ops.simple_asset_manager.load_folders()
        C.scene.simple_asset_manager.origin = False
        C.scene.simple_asset_manager.incl_cursor_rot = False
        C.scene.cursor.location = (
            random.randrange(-400.0, 400.0),
            random.randrange(-400.0, 400.0),
            random.randrange(-400.0, 400.0),
        )
        C.scene.cursor.rotation_euler = (-1.2832, -0.5664, 3.0000)

    def test_0_addon_enabled(self):
        loaded = addon_utils.check("SimpleAssetManager")
        self.assertEqual(loaded, (True, True))

    # def test_add_material(self):
    #     # add obj
    #     # select obj
    #     # add base mat
    #     # bpy.ops.simple_asset_manager.add_material()
    #     # check if material is added
    #     pass

    # def test_append_material(self):
    #     # add obj
    #     # select obj
    #     # add base mat
    #     # bpy.ops.simple_asset_manager.append_material()
    #     # check if material is appended to scene and not to obj
    #     pass

    # def test_replace_material(self):
    #     # add obj
    #     # select obj
    #     # add base mat
    #     # bpy.ops.simple_asset_manager.replace_material()
    #     # check if material is replaced by active slot
    #     pass

    # def test_add_particles(self):
    #     # bpy.ops.simple_asset_manager.add_particles()
    #     pass

    def test_append_fbx(self):
        path = os.path.join(PATH, "fbx")
        files = [x for x in os.listdir(path) if x.endswith(".fbx")]
        file = random.sample(files, 1)[0]
        select_asset(path, file)
        fp = os.path.join(path, file)
        append_test(self, fp)

    def test_append_obj(self):
        path = os.path.join(PATH, "obj")
        files = [x for x in os.listdir(path) if x.endswith(".obj")]
        file = random.sample(files, 1)[0]
        select_asset(path, file)
        fp = os.path.join(path, file)
        append_test(self, fp)

    def test_append_blend(self):
        for folder in FOLDERS[:3]:
            folder = folder.replace(" ", os.path.sep)
            path = os.path.join(PATH, folder)
            files = [x for x in os.listdir(path) if x.endswith(".blend")]
            file = random.sample(files, 1)[0]
            select_asset(path, file)
            fp = os.path.join(path, file)
            append_test(self, fp)

    # def test_link_blend(self):
    #     # bpy.ops.simple_asset_manager.link_object()
    #     pass

    # def test_open_in_new_instance(self):
    #     # ?
    #     # bpy.ops.simple_asset_manager.open_file()
    #     pass

    def test_import_hdr(self):
        path = os.path.join(PATH, "hdr")
        files = [x for x in os.listdir(path) if x.endswith(".hdr")]
        file = random.sample(files, 1)[0]
        select_asset(path, file)
        bpy.ops.simple_asset_manager.append_object()
        self.assertEqual(bpy.context.scene.world.name_full, file)

    def test_import_exr(self):
        path = os.path.join(PATH, "hdr")
        files = [x for x in os.listdir(path) if x.endswith(".exr")]
        file = random.sample(files, 1)[0]
        select_asset(path, file)
        bpy.ops.simple_asset_manager.append_object()
        self.assertEqual(bpy.context.scene.world.name_full, file)

    # def test_import_particle(self):
    #     pass

    # def test_deep_categories(self):
    #     # ?
    #     pass

    # def test_search(self):
    #     bpy.ops.simple_asset_manager.search_dynamic(items='/home/tibicen/Inne/Code/sam-private/library/hdr/dry_hay_field_1k.exr')

    # def test_render_previews(self):
    #     # TODO: solve to render part only in cycles
    #     clean_previews(PATH)
    #     bpy.context.preferences.addons['SimpleAssetManager'].preferences[
    #         'opensame'] = 1
    #     bpy.context.preferences.addons['SimpleAssetManager'].preferences[
    #         'cycles_material_previews'] = 1
    #     bpy.ops.simple_asset_manager.render_previews(
    #         sub_process=False, rerender="True", render_env="", matcap="")
    #     # check if all png are rendered
    #     for r, _, files in os.walk(PATH):
    #         for f in files:
    #             if f.endswith(('.fbx', '.obj', '.blend')):
    #                 f = os.path.splitext(f)[0] + '.png'
    #                 anwser = os.path.exists(os.path.join(r, f))
    #                 self.assertTrue(anwser)

    # check if one png is same as sample

    # def test_render_matcaps(self):
    #     #     # check if one png is same as sample
    #     pass

    # def test_imported_scene_is_deleted(self):
    #     pass

    # def test_append_to_active_collection(self):
    #     pass

    def tearDown(self):
        cleanup()
        C = bpy.context
        C.scene.simple_asset_manager.origin = False
        C.scene.simple_asset_manager.incl_cursor_rot = False
        C.scene.cursor.location = (
            random.randrange(-400.0, 400.0),
            random.randrange(-400.0, 400.0),
            random.randrange(-400.0, 400.0),
        )
        C.scene.cursor.rotation_euler = (-1.2832, -0.5664, 3.0000)


def main():
    try:
        seed = random.randrange(10000)
        random.seed(seed)
        print(f"\nTesting Simple Asset Manager [seed:{seed}]:")
        suite = unittest.defaultTestLoader.loadTestsFromTestCase(SAM_Testing)
        success = unittest.TextTestRunner().run(suite).wasSuccessful()
        if not success:
            raise Exception("Test Failed")
        sys.exit(0)
    except Exception:
        sys.exit(1)
    finally:
        pass


def add_addon_to_script_folder():
    sam_folder = None
    for m in addon_utils.modules():
        if m.bl_info["name"] == "Simple Asset Manager":
            sam_folder = os.path.dirname(m.__file__)
            # TODO: remove folder and copy folder
    fp = addon_utils.modules()[0].__file__
    p = os.path.dirname(fp)
    if p.endswith("addons"):
        if sam_folder:
            rmtree(os.path.join(p, "SimpleAssetManager"))
        copytree("SimpleAssetManager", os.path.join(p, "SimpleAssetManager"))


def prepare_blender():
    add_addon_to_script_folder()
    for addon_name in ["SimpleAssetManager", "io_scene_fbx", "io_scene_obj"]:
        addon_utils.enable(
            addon_name,
            default_set=True,
            persistent=False,
            handle_error=None,
        )

    addon_utils.modules_refresh()


if __name__ == "__main__":
    prepare_blender()
    from SimpleAssetManager.previews import purge

    main()
