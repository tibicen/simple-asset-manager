import os
import random
import requests
import string
import bpy  # type: ignore
from mathutils import Euler  # type: ignore
import addon_utils  # type: ignore


bpy.context.preferences.view.use_save_prompt = False
bpy.context.preferences.view.show_splash = False


PATH = os.path.join(os.getcwd(), "library")

FOLDERS = [
    "obiekty",
    "obiekty samochody",
    "obiekty malpy struny konie woly psy kosci gorne dziewiec dziesiec",
    "fbx",
    "obj",
    "materialy",
    "hdr",
    "particle",
]
HDRS = [
    "https://dl.polyhaven.org/file/ph-assets/HDRIs/hdr/1k/dry_hay_field_1k.hdr",
    "https://dl.polyhaven.org/file/ph-assets/HDRIs/exr/1k/dry_hay_field_1k.exr",
]
PRIMITIVES = [
    x for x in dir(bpy.ops.mesh) if x.startswith("primitive") and x.endswith("add")
]
if "primitive_circle_add" in PRIMITIVES:
    PRIMITIVES.remove("primitive_circle_add")


def euler_difference(E1, E2):
    dx = abs(E1.x - E2.x)
    dy = abs(E1.y - E2.y)
    dz = abs(E1.z - E2.z)
    return (dx, dy, dz)


def cleanup():
    for ob in bpy.data.objects:
        ob.hide_select = False
        ob.hide_render = False
        ob.hide_viewport = False
        ob.hide_set(False)
        bpy.data.objects.remove(ob)
    for coll in bpy.data.collections:
        coll.hide_select = False
        coll.hide_render = False
        coll.hide_viewport = False
    purge(bpy.data.collections)
    purge(bpy.data.objects)
    purge(bpy.data.cameras)
    purge(bpy.data.lights)
    purge(bpy.data.meshes)
    purge(bpy.data.particles)
    purge(bpy.data.materials)
    purge(bpy.data.textures)
    purge(bpy.data.images)
    purge(bpy.data.collections)


def random_material(name):
    mat = bpy.data.materials.new(name)
    mat.use_nodes = True
    BSDF = mat.node_tree.nodes["Principled BSDF"].inputs
    BSDF["Base Color"].default_value = (
        random.random(),
        random.random(),
        random.random(),
        1,
    )
    BSDF["Roughness"].default_value = random.random()
    BSDF["Specular"].default_value = random.random()
    BSDF["Clearcoat"].default_value = random.random()
    BSDF["Clearcoat Roughness"].default_value = random.random()
    return mat


def override_context():
    for window in bpy.context.window_manager.windows:
        screen = window.screen
        for area in screen.areas:
            if area.type == "VIEW_3D":
                override = {"window": window, "screen": screen, "area": area}
                bpy.ops.screen.screen_full_area(override)
                break


def create_library(path, quantity):
    # create folders
    if not os.path.exists(path):
        os.mkdir(path)
    for el in FOLDERS:
        folders = el.split(' ')
        p = path
        for f in folders:
            p = os.path.join(p, f)
            if not os.path.exists(p):
                os.mkdir(p)
    # CREATE OBJECTS
    for nr in range(quantity):
        n = random.sample(PRIMITIVES, 1)[0]
        cleanup()
        # create
        override_context()
        func = getattr(bpy.ops.mesh, n)
        func()
        name = n + "".join(random.sample(string.ascii_letters, 10))
        name = name.replace("primitive_", "")
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.context.active_object.location = (0, 0, 0)
        bpy.context.active_object.rotation_euler = Euler((0, 0, 0), "XYZ")
        blend_folder = FOLDERS[nr % 3].replace(" ", os.path.sep)
        blend_path = os.path.join(path, blend_folder, name) + ".blend"
        fbx_path = os.path.join(path, "fbx", name) + ".fbx"
        obj_path = os.path.join(path, "obj", name) + ".obj"
        bpy.ops.export_scene.obj(
            filepath=obj_path,
            filter_glob="*.obj;*.mtl",
            use_selection=True,
            axis_forward="-Z",
            axis_up="Y",
        )
        bpy.ops.export_scene.fbx(
            filepath=fbx_path, filter_glob="*.fbx", use_selection=True
        )
        ob = bpy.context.selected_objects[0]
        mat = random_material(n)
        ob.data.materials.append(mat)
        bpy.ops.wm.save_as_mainfile(filepath=blend_path, copy=True)
    # CREATE MATERIALS
    for n in range(20):
        cleanup()
        name = f"material{n}"
        mat = random_material(name)
        mat.use_fake_user = True
        blend_path = os.path.join(path, "materialy", name) + ".blend"
        bpy.ops.wm.save_as_mainfile(filepath=blend_path, copy=True)
        mat.use_fake_user = False
    # CREATE HDR EXR
    for url in HDRS:
        f = os.path.basename(url)
        dst = os.path.join(path, "hdr", f)
        r = requests.get(url, allow_redirects=True)
        open(dst, "wb").write(r.content)
    print("library created in: " + PATH)


if __name__ == "__main__":
    for addon_name in ["SimpleAssetManager", "io_scene_fbx", "io_scene_obj"]:
        addon_utils.enable(
            addon_name,
            default_set=True,
            persistent=False,
            handle_error=None,
        )
    addon_utils.modules_refresh()
    from SimpleAssetManager.previews import purge

    create_library(PATH, 10)
